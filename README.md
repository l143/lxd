![logo](./res/lxc.png)

--- 

# LXC

Es una tecnología de virtualización que utiliza el kernel de linux para poder asignar y limitar recursos así como aislar procesos (cGroups & namespaces)

Si se encuentra detrás de un proxy, debería revisar la siguiente [guía](./2_Proxy.md).

Para la instalación puede seguir esta breve [guia.](./1_Install.md)


## 🖼 Imágenes.

> **Listar** las **imágenes** disponibles **localmente**
```bash
$ lxc image list
```

> **Listar** los **repositorios** de imágenes
```bash
$ lxc remote list
```

> **Listar** las **imágenes** de un **repositorio**
```bash
$ lxc image list <repositoryName>:
```

> **Exportar** una imagen a un .tar.gz
```bash
$ lxc image export <imageName> <fileSystemPath>
```

> **Importar** una imagen de un .tar.gz
```bash
$ lxc image import <.tar.gz-Path> --alias <imageName>
```

> **Crear** una imagen a partir de un contenedor
```bash
$ lxc publish <containerName>/<version> --alias <newImageName>
``` 

## 📦 Contenedores.
> **Listar** los contenedores
```bash
$ lxc list
```


> **Crear** un nuevo contenedor
```bash
$ lxc launch <repositoryName>:<imageName> <containerName>
```

> **Ejecutar un comando** en un contenedor
```bash
$ lxc exec <containerName> <command>
```

> **Reiniciar** un contenedor
```bash
$ lxc restart <containerName>
```

> **Detener** un contenedor
```bash
$ lxc stop <containerName>
```

> **Eliminar** un contenedor
```bash
$ lxc delete <containerName>
```

---
## 📷 Snapshots  
<br> 

> **Crear** un snapshot
```bash
$ lxc snapshot <containerName> <snapshotName>
```

> **Aplicar** un snapshot a un contenedor

```bash
$ lxc restore <containerName> <snapshotName>
```

> **Eliminar** un snapshot
```bash
$ lxc delete <containerName>/<snapshotName>
```

---
## 🔩 Configuración  
<br> 

**lxc config set**  puede ser entendido como un comando general para hacer cambios en configuraciones del contenedor.


> Muestra la configuración actual
```bash
$ lxc config show
```

> Activa el autoinicio del contenedor
```bash
$ lxc config set <containerName> boot.autostart 1
```

> Delay para el inicio del contenedor
```bash
$ lxc config set <containerName> boot.autostart.delay 30
```

> Límite de memoria
```bash
$ lxc config set <containerName> limits.memory 1GB
```

## 💽 Storage
### Pool
Se refiere una unidad que puede contener varios volúmenes y pueden usarse diferentes drivers:  [btrfs](https://blog.desdelinux.net/btrfs-usarlo-no-usarlo/), [cephfs](https://ceph.io/en/), [lvm](https://puerto53.com/linux/logical-volume-manager-lvm-en-linux/), [zfs](https://www.ecured.cu/ZFS).

> **Listar** los pools existentes
```bash
$ lxc storage list
```

> **Obtener** la información de un pool.
```bash
$ lxc storage show <poolName>
```
> **Obtener** información del uso de un pool.
```bash
$ lxc storage info <poolName>
```
> **Cear** un pool con un path en concreto.
```bash
$ lxc storage create <poolname> <driver> source=<path>
```

### Volume
Se refiere a un volumen (disco) que puede ser conectado al contenedor.

> **Listar** los volúmenes
```bash
$ lxc storage volume list <poolName>
```

> **Usar** un **pool** en un contenedor (tiempo de creación)  
*El disco del contenedor se irá a dicho pool.*
```bash
$ lxc launch <imageName> <containerName> -s <poolName> 
```

## Referencias
 - https://manpages.ubuntu.com/manpages/artful/man1/lxc.storage.1.html  
 - https://documentation.ubuntu.com/lxd/en/stable-5.0/howto/storage_pools/
