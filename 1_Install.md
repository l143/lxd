# Instalación

Para instalar lxd, podemos apoyarnos del paquete disponible para Snap.

En **ubuntu**
```bash
$ sudo apt install lxd
```

En **debian 11**
```bash
$ sudo apt update
$ sudo apt install snapd
```

Ahora, instalamos con ayuda de snap.

```bash
$ snap install lxd
```

> Nota:  
Si el comando **lxd** no se encuentra disponible, es necesario agregar el directorio ***/snap/bin*** al PATH.


En **debian 12**
```bash
$ sudo apt-get -y install lxd
```

Para poder correr el comando lxd sin ser super usuario, agregamos al grupo **lxd** all usuario deseado.
```bash
$ sudo groupadd --system lxd
$ sudo usermod -G lxd -a <username>
```

Por último, debemos configurar lxd solo la primera vez.

```
$ sudo lxd init
```

