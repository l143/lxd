# Proxy

## APT
Para poder hacer instalaciones por medio de apt estando detrás de un proxy, deberá agregar la siguiente configuración en el archivo `/etc/apt/apt.conf.d/proxy.conf`. En caso de que no exista, puede crearlo.

```bash
# Http proxy
Acquire::http::Proxy "http://user:password@proxy_server:port/";
# Https proxy
Acquire::https::Proxy "http://user:password@proxy_server:port/";
```

## LXD

Con lxc para poder hacer el pull de las imágenes, y poder crear un contenedor, hay que hacer la siguiente configuación.

```bash
# Http proxy
lxc config set core.proxy_http http://user:password@proxy_server:port/
# Https proxy
lxc config set core.proxy_https http://user:password@proxy_server:port/
```

## Referencia
- https://installati.one/install-lxd-debian-12/
- https://linuxiac.com/how-to-use-apt-with-proxy/